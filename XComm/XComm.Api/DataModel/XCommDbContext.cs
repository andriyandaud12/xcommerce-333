﻿using Microsoft.EntityFrameworkCore;

namespace XComm.Api.DataModel
{
    public class XCommDbContext: DbContext
    {
        public XCommDbContext(DbContextOptions<XCommDbContext> options) : base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Variants> Variant { get; set; }
        public DbSet<Products> Product { get; set; }
        public DbSet<OrderHeaders> OrderHeaders { get; set; }
        public DbSet<OrderDetails> OrderDetail { get; set; }
        public DbSet<FileCollections> FileCollection { get; set; }
        public DbSet<Accounts> Account { get; set; }
        public DbSet<UserRoles> UserRole { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<RoleGroup> RoleGroup { get; set; }
        public DbSet<AuthorizationGroup> AuthorizationGroup { get; set; }

        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.Seed();

            modelBuilder.Entity<Category>()
                .HasIndex(o => o.Initial)
                .IsUnique();
            
            modelBuilder.Entity<Category>()
                .HasIndex(o => o.Name)
                .IsUnique();

            modelBuilder.Entity<Variants>()
                .HasIndex(o => o.Initial)
                .IsUnique();

            modelBuilder.Entity<Variants>()
                .HasIndex(o => o.Name)
                .IsUnique();

            modelBuilder.Entity<Products>()
                .HasIndex(o => o.Initial)
                .IsUnique();

            modelBuilder.Entity<Products>()
                .HasIndex(o => o.Name)
                .IsUnique();

            modelBuilder.Entity<OrderHeaders>()
                .HasIndex(o => o.Reference)
                .IsUnique();

            modelBuilder.Entity<FileCollections>()
                .HasIndex(o => o.Title)
                .IsUnique();

            modelBuilder.Entity<FileCollections>()
                .HasIndex(o => o.FileName)
                .IsUnique();

            modelBuilder.Entity<Accounts>()
                .HasIndex(o => o.UserName)
                .IsUnique();

            modelBuilder.Entity<Products>()
                .Property(o => o.Price)
                .HasColumnType("decimal(18, 4)");
            
            modelBuilder.Entity<Products>()
                .Property(o => o.Stock)
                .HasColumnType("decimal(18, 4)");

            modelBuilder.Entity<Products>()
                .Property(o => o.GalleryId)
                .IsRequired(false);

            modelBuilder.Entity<OrderHeaders>()
                .Property(o => o.Amount)
                .HasColumnType("decimal(18, 4)");
            
            modelBuilder.Entity<OrderDetails>()
                .Property(o => o.Quantity)
                .HasColumnType("decimal(18, 4)");

            modelBuilder.Entity<OrderDetails>()
                .Property(o => o.Price)
                .HasColumnType("decimal(18, 4)");

            modelBuilder.Entity<Gallery>()
                .Property(o => o.Base64Big)
                .HasColumnType("nvarchar(max)");

            modelBuilder.Entity<Gallery>()
                .Property(o => o.Base64Small)
                .HasColumnType("nvarchar(max)");

            modelBuilder.Entity<Accounts>()
                .HasIndex( o => o.UserName)
                .IsUnique();

            modelBuilder.Entity<Accounts>()
                .Property(o => o.Otp)
                .HasColumnType("char(6)");

            modelBuilder.Entity<RoleGroup>()
                .HasIndex(o => o.GroupName)
                .IsUnique();

            modelBuilder.Entity<Accounts>()
                .Property(o => o.Email)
                .HasDefaultValue("andriyandaud12@gmail.com");
            
            modelBuilder.Entity<Accounts>()
                .Property(o => o.RoleGroupId)
                .HasDefaultValue(1);
        }

        
    }
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accounts>()
                .HasData(
                new Accounts()
                {
                    Id = 1,
                    UserName = "admin",
                    Password = "ac9689e2272427085e35b9d3e3e8bed88cb3434828b43b86fc0596cad4c6e270",
                    FirstName = "Super",
                    LastName = "Admin",
                    Email = "andriyandaud12@gmail.com",
                    Active = true,
                    CreatedDate = DateTime.Now,
                    CreatedBy = "admin"
                },
                new Accounts()
                {
                    Id = 2,
                    UserName = "user1",
                    Password = "0a041b9462caa4a31bac3567e0b6e6fd9100787db2ab433d96f6d178cabfce90",
                    FirstName = "User",
                    LastName = "One",
                    Email = "andriyandaud12@gmail.com",
                    Active = true,
                    CreatedDate = DateTime.Now,
                    CreatedBy = "admin"
                });

            modelBuilder.Entity<RoleGroup>()
                .HasData(
                    new RoleGroup() { Id = 1, GroupName = "Customer", CreatedBy = "Admin", CreatedDate = DateTime.Now },
                    new RoleGroup() { Id = 2, GroupName = "Kasir", CreatedBy = "Admin", CreatedDate = DateTime.Now }
                );

            modelBuilder.Entity<AuthorizationGroup>()
                .HasData(
                    new AuthorizationGroup() { Id = 1, RoleGroupId = 1, Role = "Products", CreatedBy = "Admin", CreatedDate = DateTime.Now },
                    new AuthorizationGroup() { Id = 2, RoleGroupId = 1, Role = "Orders", CreatedBy = "Admin", CreatedDate = DateTime.Now },
                    new AuthorizationGroup() { Id = 3, RoleGroupId = 2, Role = "Categories", CreatedBy = "Admin", CreatedDate = DateTime.Now },
                    new AuthorizationGroup() { Id = 4, RoleGroupId = 2, Role = "Variants", CreatedBy = "Admin", CreatedDate = DateTime.Now },
                    new AuthorizationGroup() { Id = 5, RoleGroupId = 2, Role = "Products", CreatedBy = "Admin", CreatedDate = DateTime.Now },
                    new AuthorizationGroup() { Id = 6, RoleGroupId = 2, Role = "Orders", CreatedBy = "Admin", CreatedDate = DateTime.Now }

                );
        }
    }
}
