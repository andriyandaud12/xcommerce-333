﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace XComm.Api.DataModel
{
    [Table("MasterFileCollecitons")]
    public class FileCollections: BaseSchema
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, MaxLength(50)]
        public string Title { get; set; }

        [Required, MaxLength(200)]
        public string FileName { get; set; }
        public bool Active { get; set; }
    }
}
