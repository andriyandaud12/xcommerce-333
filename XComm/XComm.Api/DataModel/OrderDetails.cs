﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace XComm.Api.DataModel
{
    [Table("MasterOrderDetails")]
    public class OrderDetails: BaseSchema
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long HeaderId { get; set; }

        public long ProductId { get; set; }

        public decimal Quantity { get; set; }

        public decimal Price { get; set; }

        public bool Active { get; set; }

        [ForeignKey("HeaderId")]
        public virtual OrderHeaders OrderHeader { get; set; }
        
        [ForeignKey("ProductId")]
        public virtual Products Products { get; set; }
    }
}
