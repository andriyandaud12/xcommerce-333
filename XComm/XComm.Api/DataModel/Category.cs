﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XComm.Api.DataModel
{
    //Entitiy Category
    //Inheritance mengambil sifat dari sebuah class contoh = Category: BaseSchema
    [Table("MasterCategories")]
    public class Category: BaseSchema
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, MaxLength(10)]
        public string Initial { get; set; }

        [Required, MaxLength(50)]
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}
