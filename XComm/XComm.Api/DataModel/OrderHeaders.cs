﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace XComm.Api.DataModel
{
    [Table("MasterOrderHeader")]
    public class OrderHeaders: BaseSchema
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        
        [Required, MaxLength(15),]
        public string Reference { get; set; }

        public decimal Amount { get; set; }

        public bool Active { get; set; }
    }
}
