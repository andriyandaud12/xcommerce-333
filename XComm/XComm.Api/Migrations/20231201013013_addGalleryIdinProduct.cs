﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XComm.Api.Migrations
{
    /// <inheritdoc />
    public partial class addGalleryIdinProduct : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "GalleryId",
                table: "MasterProducts",
                type: "bigint",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2023, 12, 1, 8, 30, 12, 866, DateTimeKind.Local).AddTicks(3572));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 12, 1, 8, 30, 12, 866, DateTimeKind.Local).AddTicks(3584));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 1, 8, 30, 12, 866, DateTimeKind.Local).AddTicks(3731));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 1, 8, 30, 12, 866, DateTimeKind.Local).AddTicks(3732));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 1, 8, 30, 12, 866, DateTimeKind.Local).AddTicks(3733));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 1, 8, 30, 12, 866, DateTimeKind.Local).AddTicks(3734));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 1, 8, 30, 12, 866, DateTimeKind.Local).AddTicks(3735));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 6L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 1, 8, 30, 12, 866, DateTimeKind.Local).AddTicks(3735));

            migrationBuilder.UpdateData(
                table: "MasterRoleGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 1, 8, 30, 12, 866, DateTimeKind.Local).AddTicks(3714));

            migrationBuilder.UpdateData(
                table: "MasterRoleGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 1, 8, 30, 12, 866, DateTimeKind.Local).AddTicks(3715));

            migrationBuilder.CreateIndex(
                name: "IX_MasterProducts_GalleryId",
                table: "MasterProducts",
                column: "GalleryId");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProducts_MasterGalleries_GalleryId",
                table: "MasterProducts",
                column: "GalleryId",
                principalTable: "MasterGalleries",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MasterProducts_MasterGalleries_GalleryId",
                table: "MasterProducts");

            migrationBuilder.DropIndex(
                name: "IX_MasterProducts_GalleryId",
                table: "MasterProducts");

            migrationBuilder.DropColumn(
                name: "GalleryId",
                table: "MasterProducts");

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3198));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3209));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3321));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3322));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3323));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3324));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3325));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 6L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3325));

            migrationBuilder.UpdateData(
                table: "MasterRoleGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3307));

            migrationBuilder.UpdateData(
                table: "MasterRoleGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3308));
        }
    }
}
