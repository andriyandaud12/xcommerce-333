﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XComm.Api.Migrations
{
    /// <inheritdoc />
    public partial class addAuthorizationGroup : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Attempt",
                table: "MasterAccounts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Otp",
                table: "MasterAccounts",
                type: "char(6)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RoleGroupId",
                table: "MasterAccounts",
                type: "bigint",
                nullable: false,
                defaultValue: 1L);

            migrationBuilder.CreateTable(
                name: "MasterRoleGroups",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GroupName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterRoleGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MasterAuthorizationGroup",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleGroupId = table.Column<long>(type: "bigint", nullable: false),
                    role = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterAuthorizationGroup", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MasterAuthorizationGroup_MasterRoleGroups_RoleGroupId",
                        column: x => x.RoleGroupId,
                        principalTable: "MasterRoleGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "admin",
                columns: new[] { "Attempt", "CreatedDate", "Otp" },
                values: new object[] { 0, new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2591), null });

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                columns: new[] { "Attempt", "CreatedDate", "Otp" },
                values: new object[] { 0, new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2606), null });

            migrationBuilder.InsertData(
                table: "MasterRoleGroups",
                columns: new[] { "Id", "CreatedBy", "CreatedDate", "GroupName", "ModifiedBy", "ModifiedDate" },
                values: new object[,]
                {
                    { 1L, "Admin", new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2712), "Customer", null, null },
                    { 2L, "Admin", new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2713), "Kasir", null, null }
                });

            migrationBuilder.InsertData(
                table: "MasterAuthorizationGroup",
                columns: new[] { "Id", "CreatedBy", "CreatedDate", "ModifiedBy", "ModifiedDate", "RoleGroupId", "role" },
                values: new object[,]
                {
                    { 1L, "Admin", new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2726), null, null, 1L, "Products" },
                    { 2L, "Admin", new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2727), null, null, 1L, "Orders" },
                    { 3L, "Admin", new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2728), null, null, 2L, "Categories" },
                    { 4L, "Admin", new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2728), null, null, 2L, "Variants" },
                    { 5L, "Admin", new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2730), null, null, 2L, "Products" },
                    { 6L, "Admin", new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2731), null, null, 2L, "Orders" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MasterAccounts_RoleGroupId",
                table: "MasterAccounts",
                column: "RoleGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterAuthorizationGroup_RoleGroupId",
                table: "MasterAuthorizationGroup",
                column: "RoleGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_MasterRoleGroups_GroupName",
                table: "MasterRoleGroups",
                column: "GroupName",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MasterAccounts_MasterRoleGroups_RoleGroupId",
                table: "MasterAccounts",
                column: "RoleGroupId",
                principalTable: "MasterRoleGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MasterAccounts_MasterRoleGroups_RoleGroupId",
                table: "MasterAccounts");

            migrationBuilder.DropTable(
                name: "MasterAuthorizationGroup");

            migrationBuilder.DropTable(
                name: "MasterRoleGroups");

            migrationBuilder.DropIndex(
                name: "IX_MasterAccounts_RoleGroupId",
                table: "MasterAccounts");

            migrationBuilder.DropColumn(
                name: "Attempt",
                table: "MasterAccounts");

            migrationBuilder.DropColumn(
                name: "Otp",
                table: "MasterAccounts");

            migrationBuilder.DropColumn(
                name: "RoleGroupId",
                table: "MasterAccounts");

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 28, 15, 14, 37, 530, DateTimeKind.Local).AddTicks(9019));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 28, 15, 14, 37, 530, DateTimeKind.Local).AddTicks(9030));
        }
    }
}
