﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XComm.Api.Migrations
{
    /// <inheritdoc />
    public partial class addEmail : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "MasterAccounts",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValue: "andriyandaud12@gmail.com");

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "admin",
                columns: new[] { "CreatedDate", "Email" },
                values: new object[] { new DateTime(2023, 11, 28, 15, 14, 37, 530, DateTimeKind.Local).AddTicks(9019), "andriyandaud12@gmail.com" });

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                columns: new[] { "CreatedDate", "Email" },
                values: new object[] { new DateTime(2023, 11, 28, 15, 14, 37, 530, DateTimeKind.Local).AddTicks(9030), "andriyandaud12@gmail.com" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "MasterAccounts");

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 28, 14, 45, 20, 560, DateTimeKind.Local).AddTicks(4416));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 28, 14, 45, 20, 560, DateTimeKind.Local).AddTicks(4427));
        }
    }
}
