﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XComm.Api.Migrations
{
    /// <inheritdoc />
    public partial class updateAuthorizationGroup : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "role",
                table: "MasterAuthorizationGroup",
                newName: "Role");

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3198));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3209));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3321));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3322));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3323));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3324));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3325));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 6L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3325));

            migrationBuilder.UpdateData(
                table: "MasterRoleGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3307));

            migrationBuilder.UpdateData(
                table: "MasterRoleGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 41, 32, 47, DateTimeKind.Local).AddTicks(3308));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Role",
                table: "MasterAuthorizationGroup",
                newName: "role");

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2591));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2606));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2726));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2727));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2728));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2728));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2730));

            migrationBuilder.UpdateData(
                table: "MasterAuthorizationGroup",
                keyColumn: "Id",
                keyValue: 6L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2731));

            migrationBuilder.UpdateData(
                table: "MasterRoleGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2712));

            migrationBuilder.UpdateData(
                table: "MasterRoleGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 9, 27, 40, 730, DateTimeKind.Local).AddTicks(2713));
        }
    }
}
