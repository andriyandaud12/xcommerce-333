﻿using ViewModel;
using XComm.Api.DataModel;

namespace XComm.Api.Repositories
{
    public class OrderHeadersRepository : IRepository<OrderHeadersViewModel>
    {
        private XCommDbContext _dbContext; /*= new XCommDbContext();*/
        public OrderHeadersRepository(XCommDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public OrderHeadersViewModel ChangeStatus(long id, bool status)
        {
            throw new NotImplementedException();
        }

        public OrderHeadersViewModel Create(OrderHeadersViewModel model)
        {
            OrderHeadersViewModel result = new OrderHeadersViewModel();
            try
            {
                string newRef = NewReference();
                if (!string.IsNullOrEmpty(newRef))
                {
                    model.Reference = newRef;
                    using var trans = _dbContext.Database.BeginTransaction();

                    OrderHeaders entOh = new OrderHeaders();
                    entOh.Reference = model.Reference;
                    //entOh.Amount = model.Amount;
                    entOh.Active = model.Active;

                    entOh.CreatedBy = "Daud";
                    entOh.CreatedDate = DateTime.Now;

                    _dbContext.OrderHeaders.Add(entOh);
                    _dbContext.SaveChanges();
                    decimal amount = 0;

                    foreach (var item in model.Details)
                    {
                        OrderDetails entOd = new OrderDetails();
                        entOd.HeaderId = item.Id;
                        entOd.ProductId = item.ProductId;
                        entOd.Quantity = item.Quantity;
                        entOd.Price = item.Price;

                        entOh.CreatedBy = "Daud";
                        entOh.CreatedDate = DateTime.Now;

                        _dbContext.OrderDetail.Add(entOd);

                        amount += item.Price * item.Quantity;
                    }
                    entOh.Amount = amount;
                    _dbContext.OrderHeaders.Update(entOh);
                    _dbContext.SaveChanges();

                    trans.Commit();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<OrderHeadersViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public OrderHeadersViewModel GetById(long id)
        {
            throw new NotImplementedException();
        }

        public List<OrderHeadersViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            throw new NotImplementedException();
        }

        public OrderHeadersViewModel Update(OrderHeadersViewModel model)
        {
            throw new NotImplementedException();
        }

        private string NewReference()
        {
            //    yyMM incr
            //SLS-2311-0123
            string yearMonth = DateTime.Now.ToString("yy") + DateTime.Now.Month.ToString("D2");//2311
            string newRef = "SLS-" + yearMonth + "-"; //SLS-2311
            try
            {
                var maxRef = _dbContext.OrderHeaders
                    .Where(o => o.Reference.Contains(newRef))
                    .OrderByDescending(o => o.Reference)
                    .FirstOrDefault();

                if (maxRef != null)
                {
                    //SLS-2311-0120
                    string[] oldRef = maxRef.Reference.Split('-');
                    int newInc = int.Parse(oldRef[2]) + 1; //0121
                    newRef += newInc.ToString("D4");//SLS-2311-0003
                } else
                {
                    newRef += "0001"; //SLS-2311-0001
                }
            } 
            catch (Exception)
            {
                newRef = string.Empty;
            }
            return newRef;
        }
    }
}
