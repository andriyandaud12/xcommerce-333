﻿using Framework.Auth;
using System;
using ViewModel;
using XComm.Api.DataModel;

namespace XComm.Api.Repositories
{
    public class AccountRepository
    {
        private XCommDbContext _dbContext;
        public AccountRepository(XCommDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<AccountViewModel> GetAccount()
        {
            List<AccountViewModel> result = new List<AccountViewModel>();
            try
            {
                result = (from o in _dbContext.Accounts
                          select new AccountViewModel
                          {
                              Id = o.Id,
                              UserName = o.UserName,
                              FirstName = o.FirstName,
                              LastName = o.LastName,
                              Email = o.Email,
                              Active = o.Active
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public AccountViewModel Authentication(LoginViewModel model)
        {
            AccountViewModel result = new AccountViewModel();
            Accounts account = _dbContext.Accounts
                .Where(o => o.UserName == model.UserName && o.Password == Encryption.HashSha256(model.Password)).FirstOrDefault();
                if ( account != null)
                {
                result = new AccountViewModel
                {
                    Id = account.Id,
                    UserName = account.UserName,
                    FirstName = account.FirstName,
                    LastName = account.LastName,
                    Email = account.Email,
                    //Roles = Roles(o.RoleGroupId),
                    Active = account.Active,
                };
                result.Roles = Roles(account.RoleGroupId);
                }
            return result;
        }

        public AccountViewModel Authentication(RegisterViewModel model)
        {
            AccountViewModel result = new AccountViewModel();
            try
            {
                Accounts entity = new Accounts();
                entity.UserName = model.UserName;
                entity.FirstName = model.FirstName;
                entity.LastName = model.LastName;
                entity.Password = Encryption.HashSha256(model.Password);
                entity.Email = model.Email;

                entity.CreatedBy = "Daud";
                entity.CreatedDate = DateTime.Now;

                _dbContext.Accounts.Add(entity);
                _dbContext.SaveChanges();

                model.UserName = entity.UserName;
                result = (from o in _dbContext.Accounts
                          where o.UserName == model.UserName  
                          select new AccountViewModel
                          {
                              Id = o.Id,
                              UserName = o.UserName,
                              FirstName = o.FirstName,
                              LastName = o.LastName,
                              Email = o.Email,
                              
                          }).FirstOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        private List<string> Roles(long roleGroupId)
        {
            List<string> result = new List<string>();
            var list = _dbContext.AuthorizationGroup
                .Where(o =>  o.RoleGroupId == roleGroupId)
                .ToList();
            foreach (var group in list)
            {       
                result.Add(group.Role);
            }
            return result;
        }
    }   
}
