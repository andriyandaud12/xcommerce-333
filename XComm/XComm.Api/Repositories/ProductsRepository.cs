﻿using ViewModel;
using XComm.Api.DataModel;

namespace XComm.Api.Repositories
{
    public class ProductsRepository : IRepository<ProductsViewModel>
    {
        private XCommDbContext _dbContext; /*= new XCommDbContext();*/
        private ResponseResult _result = new ResponseResult();
        public ProductsRepository(XCommDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ProductsViewModel ChangeStatus(long id, bool status)
        {
            ProductsViewModel result = new ProductsViewModel();
            try
            {
                Products entity = _dbContext.Product
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Active = status;
                    entity.ModifiedBy = "Daud";
                    entity.ModifiedDate = DateTime.Now;

                    _dbContext.SaveChanges();

                    result = new ProductsViewModel()
                    {
                        Id = entity.Id,
                        VariantId = entity.VariantId,
                        GalleryId = entity.GalleryId,
                        Initial = entity.Initial,
                        Name = entity.Name,
                        Description = entity.Description,
                        Price = entity.Price,
                        Stock = entity.Stock,
                        Active = status,
                    };
                }
                //else
                //{
                //    _result.Success = false;
                //    _result.Message = "Products Not Found!!";
                //}
            }
            catch (Exception e)
            {
                //_result.Success = false;
                //_result.Message = e.Message;
                //throw;
            }
            return result;
        }

        public ProductsViewModel Create(ProductsViewModel model)
        {
            ProductsViewModel result = new ProductsViewModel();
            try
            {
                Products entity = new Products();
                entity.VariantId = model.VariantId;
                entity.GalleryId = model.GalleryId;
                entity.Initial = model.Initial;
                entity.Name = model.Name;
                entity.Description = model.Description;
                entity.Price = model.Price;
                entity.Stock = model.Stock;
                entity.Active = model.Active;

                entity.CreatedBy = "Daud";
                entity.CreatedDate = DateTime.Now;

                _dbContext.Product.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;
            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<ProductsViewModel> GetAll()
        {
            List<ProductsViewModel> result =    new List<ProductsViewModel>();
            try
            {
                result = (from o in _dbContext.Product
                          select new ProductsViewModel
                          {
                              Id = o.Id,
                              VariantId = o.VariantId,
                              Variants = new VariantsViewModel()
                              {
                                  Id = o.Variants.Id,
                                  Initial = o.Variants.Initial,
                                  Name = o.Variants.Name,
                                  Active = o.Variants.Active,
                                  Category = new CategoryViewModel()
                                  {
                                      Id = o.Variants.Category.Id,
                                      Initial = o.Variants.Category.Initial,
                                      Name = o.Variants.Category.Name,
                                      Active = o.Variants.Category.Active,
                                  }
                              },
                              GalleryId = o.GalleryId,
                              Gallery = new GalleryViewModel ()
                              {
                                  Id = o.Gallery.Id,
                                  Title = o.Gallery.Title,
                                  Base64Big = o.Gallery.Base64Big,
                                  Base64Small = o.Gallery.Base64Small,
                                  Active = o.Gallery.Active,
                              },
                              Initial = o.Initial,
                              Name = o.Name,
                              Description = o.Description,
                              Price = o.Price,
                              Stock = o.Stock,
                              Active = o.Active
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public ProductsViewModel GetById(long id)
        {
            ProductsViewModel result = new ProductsViewModel();
            try
            {
                result = (from o in _dbContext.Product
                          where o.Id == id
                          select new ProductsViewModel
                          {
                              Id = o.Id,
                              Variants = new VariantsViewModel()
                              {
                                  Id = o.Variants.Id,
                                  Initial = o.Variants.Initial,
                                  Name = o.Variants.Name,
                                  Active = o.Variants.Active
                              },
                              GalleryId = o.GalleryId,
                              Gallery = new GalleryViewModel()
                              {
                                  Id = o.Gallery.Id,
                                  Title = o.Gallery.Title,
                                  Base64Big = o.Gallery.Base64Big,
                                  Base64Small = o.Gallery.Base64Small,
                                  Active = o.Gallery.Active,
                              },
                              Base64 = o.Gallery.Base64Small,
                              VariantId = o.VariantId,
                              Initial = o.Initial,
                              Name = o.Name,
                              Description = o.Description,
                              Price = o.Price,
                              Stock = o.Stock,
                              Active = o.Active
                          }).FirstOrDefault();
                if (result == null)
                {
                    return new ProductsViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public List<ProductsViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            //throw new NotImplementedException();
            List<ProductsViewModel> result = new List<ProductsViewModel>();
            try
            {
                // Filter Search
                var query = _dbContext.Product
                    .Where(o => o.Initial.Contains(search) || o.Name.Contains(search));

                int count = query.Count();

                if (count > 0)
                {
                    switch (orderBy)
                    {
                        case "initial":
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Initial) :
                                query.OrderByDescending(o => o.Initial);
                            break;
                        case "name":
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Name) :
                                query.OrderByDescending(o => o.Name);
                            break;
                        default:
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Id) :
                                query.OrderByDescending(o => o.Id);
                            break;
                    }
                    _result.Data = query
                        .Skip(pageNum - 1)
                        .Take(rows)
                        .Select(o => new ProductsViewModel
                        {
                            Id = o.Id,
                            VariantId = o.VariantId,
                            Variants = new VariantsViewModel()
                            {
                                Id = o.Variants.Id,
                                Initial = o.Variants.Initial,
                                Name = o.Variants.Name,
                                Active = o.Variants.Active,
                                Category = new CategoryViewModel()
                                {
                                    Id = o.Variants.Category.Id,
                                    Initial = o.Variants.Category.Initial,
                                    Name = o.Variants.Category.Name,
                                    Active = o.Variants.Category.Active,
                                }
                            },
                            Initial = o.Initial,
                            Name = o.Name,
                            Description = o.Description,
                            Price = o.Price,
                            Stock = o.Stock,
                            Active = o.Active,
                            GalleryId = o.GalleryId,
                            Base64 = o.Gallery.Base64Small,
                            Gallery = o.Gallery != null ? new GalleryViewModel()
                            {
                                Id = o.Gallery.Id | 0,
                                Title = o.Gallery.Title,
                                Base64Big = o.Gallery.Base64Big,
                                Base64Small = o.Gallery.Base64Small,
                                Active = o.Gallery.Active
                            } : new GalleryViewModel()
                        }).ToList();
                    _result.Pages = (int)Math.Ceiling((decimal)count / (decimal)rows);

                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Message = "Not Record Found";
                }
            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
                throw;
            }
            return _result;
        }

        public ProductsViewModel Update(ProductsViewModel model)
        {
            try
            {
                Products entity = _dbContext.Product
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Initial = model.Initial;
                    entity.Name = model.Name;
                    entity.Description = model.Description;
                    entity.VariantId = model.VariantId;
                    entity.Price = model.Price;
                    entity.Stock = model.Stock;
                    entity.GalleryId = model.GalleryId;

                    entity.ModifiedBy = "Daud";
                    entity.ModifiedDate = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    //_result.Success = false;
                    //_result.Message = "Products Not Found!!";
                    //_result.Data = model;
                }
            }
            catch (Exception e)
            {
                //_result.Success = false;
                //_result.Message = e.Message;
                //throw;
            }
            return model;
        }

        public ProductsViewModel ChangeGallery(long id, long galleryId)
        {
            ProductsViewModel result = new ProductsViewModel();
            try
            {
                Products product = _dbContext.Product
                    .Where(p => p.Id == id)
                    .FirstOrDefault();
                if (product != null)
                {
                    product.GalleryId = galleryId;
                    _dbContext.SaveChanges();

                    result = new ProductsViewModel()
                    {
                        Id = id,
                        GalleryId = galleryId,
                        Initial = product.Initial,
                        Gallery = new GalleryViewModel()
                    };
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

    }
}