﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Repositories;

namespace XComm.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private OrderHeadersRepository _repo; /*= new VariantsRepository();*/
        public OrderController(XCommDbContext dbContext)
        {
            _repo = new OrderHeadersRepository(dbContext);
        }

        [HttpPost]
        public async Task<OrderHeadersViewModel> Post(OrderHeadersViewModel model)
        {
            return _repo.Create(model);
        }

        //    [HttpGet]
        //    public async Task<List<OrderViewModel>> Get()
        //    {
        //        return _repo.GetAll();
        //    }

        //    [HttpGet("{id}")]
        //    public async Task<OrderViewModel> Get(long id)
        //    {
        //        return _repo.GetById(id);
        //    }
    }
}
