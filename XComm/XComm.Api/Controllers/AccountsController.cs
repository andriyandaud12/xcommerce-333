﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Repositories;

namespace XComm.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private AccountRepository _repo;
        public AccountsController(XCommDbContext dbContext, IConfiguration configuration)
        {
            _configuration = configuration;
            _repo = new AccountRepository(dbContext);
        }

        [HttpPost]
        public async Task<AccountViewModel> Post(LoginViewModel model)
        {
            AccountViewModel result = new AccountViewModel();
            if (ModelState.IsValid)
            {
                result = _repo.Authentication(model);
                if (result != null)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, result.UserName),
                        new Claim("FirstName", result.FirstName),
                        new Claim("LastName", result.LastName)
                    };

                    foreach (var role in result.Roles)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, role));
                    }

                    var token = GetToken(claims);
                    result.Token = new JwtSecurityTokenHandler().WriteToken(token);
                } else
                {
                    return null;
                }
            }
            return result;
        }

        [HttpPost("Register")]
        public async Task<AccountViewModel> Post(RegisterViewModel model)
        {
            return _repo.Authentication(model);
        }

        [HttpGet]
        public async Task<List<AccountViewModel>> GetAccount()

        {
            return _repo.GetAccount();   
        }

        private JwtSecurityToken GetToken(List<Claim> claims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:Issuer"],
                audience: _configuration["JWT:Audience"],
                expires: DateTime.Now.AddDays(Convert.ToDouble(_configuration["JWT:Expires"])),
                claims: claims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );
            return token;
        }
    }
}
