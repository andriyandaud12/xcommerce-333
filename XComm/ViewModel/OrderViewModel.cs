﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class OrderHeadersViewModel
    {
        public long Id { get; set; }

        public string Reference { get; set; }

        public decimal Amount { get; set; }

        public bool Active { get; set; }

        public List<OrderDetailsViewModel> Details { get; set; }
    }

    public class OrderDetailsViewModel
    {
        public OrderDetailsViewModel()
        {
            Quantity = 1;
        }
        public long Id { get; set; }

        public long HeaderId { get; set; }
        public OrderHeadersViewModel? OrderHeader { get; set; }

        public long ProductId { get; set; }
        public ProductsViewModel? Products { get; set; }

        public decimal Quantity { get; set; }

        public decimal Price { get; set; }

        public bool Active { get; set; }
    }
}
