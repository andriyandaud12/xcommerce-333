import axios from "axios";
import { config } from "../configurations/config";
import { ICategory } from "../interface/iCategory";
import { IPagination } from "../interface/iPagination";

export const CategoryService = {
  getAll: (pg: IPagination) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(
        config.apiUrl +
          `/Category?pageNum=${pg.pageNum}&rows=${pg.rows}${searchStr}&orderBy=${pg.orderBy}&sort=${pg.sort}`
      )
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },

  getAllCat: async () => {
    const results = await axios
      .get(config.apiUrl + "/Category?sort=0&pageNum=1&rows=500&orderBy=name")
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return results;
  },

  post: (category: ICategory) => {
    const result = axios
      .post(config.apiUrl + "/Category", category)
      .then((respons) => {
        // console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  update: (id: number, category: ICategory) => {
    const result = axios
      .put(config.apiUrl + "/Category/" + id, category)
      .then((respons) => {
        // console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  changeStatus: (id: number, status: boolean) => {
    const result = axios
      .put(config.apiUrl + `/Category/changestatus/${id}?status=${status}`)
      .then((respons) => {
        // console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  getById: (id: number) => {
    const result = axios
      .get(config.apiUrl + "/Category/" + id)
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
};
