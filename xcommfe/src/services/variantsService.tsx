import axios from "axios";
import { config } from "../configurations/config";
import { IVariants } from "../interface/iVariants";
import { IPagination } from "../interface/iPagination";

export const VariantsService = {
  getAll: (pg: IPagination) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : ``;
    const result = axios
      .get(
        config.apiUrl +
          `/Variants?pageNum=${pg.pageNum}&rows=${pg.rows}${searchStr}&orderBy=${pg.orderBy}&sort=${pg.sort}`
      )
      .then((respons) => {
        console.log(respons);
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },

  getByParentId: (id: number) => {
    console.log(config.apiUrl + "/Variants/category/" + id);
    const result = axios
      .get(config.apiUrl + "/Variants/category/" + id)
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  post: (variants: IVariants) => {
    const { category, ...newVariant } = variants;
    const result = axios
      .post(config.apiUrl + "/Variants", newVariant)
      .then((respons) => {
        // console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  update: (id: number, variants: IVariants) => {
    const result = axios
      .put(config.apiUrl + "/Variants/" + id, variants)
      .then((respons) => {
        // console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  changeStatus: (id: number, status: boolean) => {
    const result = axios
      .put(config.apiUrl + `/Variants/changestatus/${id}?status=${status}`)
      .then((respons) => {
        // console.log(respons);
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  getById: (id: number) => {
    const result = axios
      .get(config.apiUrl + "/Variants/" + id)
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
};
