import React from "react";
import { IProducts } from "../../interface/iProducts";
import { ProductsService } from "../../services/productsService";

import Form from "../products/form";
import { config } from "../../configurations/config";
import { Ecommand } from "../../enums/eCommand";

import { IVariants } from "../../interface/iVariants";
import { VariantsService } from "../../services/variantsService";
import { IPagination } from "../../interface/iPagination";
import { ICategory } from "../../interface/iCategory";
import { CategoryService } from "../../services/categoryService";
import { IGallery } from "../../interface/iGallery";
import { GalleryService } from "../../services/galleryService";
import GalleryGrid from "../gallery/galleryGrid";

interface IProps {}

interface IState {
  categories: ICategory[];
  variants: IVariants[];
  products: IProducts[];
  galleries: IGallery[];
  product: IProducts;
  command: Ecommand;
  showModal: boolean;
  pagination: IPagination;
  imagePreview: any;
  showGallery: boolean;
}

export default class Products extends React.Component<IProps, IState> {
  newPagination: IPagination = {
    pageNum: 1,
    rows: config.rowsPerPage[0],
    search: "",
    orderBy: "id",
    sort: 1,
    pages: 0,
  };

  newCategory: ICategory = {
    id: 0,
    initial: "",
    name: "",
    active: false,
  };

  newVariant: IVariants = {
    id: 0,
    categoryId: 0,
    initial: "",
    name: "",
    active: false,
    category: this.newCategory,
  };

  newGallery: IGallery = {
    id: 0,
    title: "",
    base64Big: "",
    base64Small: "",
    active: false,
  };

  newProduct: IProducts = {
    id: 0,
    categoryId: 0,
    variantId: 0,
    galleryId: 0,
    gallery: this.newGallery,
    initial: "",
    name: "",
    description: "",
    price: 0,
    stock: 0,
    active: false,
    variants: this.newVariant,
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      categories: [],
      variants: [],
      products: [],
      galleries: [],
      product: this.newProduct,
      showModal: false,
      command: Ecommand.create,
      pagination: this.newPagination,
      imagePreview: config.noImage,
      showGallery: false,
    };
  }

  componentDidMount(): void {
    this.loadProducts();
  }

  loadGalleries = async () => {
    const result = await GalleryService.getAllGal();
    if (result.success) {
      this.setState({
        galleries: result.result,
      });
    } else {
      alert("Error" + result.result);
    }
  };

  loadProducts = async () => {
    const { pagination } = this.state;
    const result = await ProductsService.getAll(pagination);
    if (result.success) {
      const catResult = await CategoryService.getAllCat();
      if (catResult.success) {
        this.setState({
          categories: catResult.result.data,
        });
      }
      const galResult = await GalleryService.getAllGal();
      if (catResult.success) {
        this.setState({
          galleries: galResult.result.data,
        });
      }
      this.setState({
        products: result.result,
        pagination: {
          ...this.state.pagination,
          pages: result.pages,
        },
      });
    } else {
      alert("Error: " + result.result);
    }
  };

  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
      showGallery: val,
    });
    console.log(this.state.showModal);
  };

  changeHandler = (name: any) => (event: any) => {
    if (name === "categoryId") {
      VariantsService.getByParentId(event.target.value).then((result) => {
        this.setState({
          variants: result.result,
        });
      });
    }
    this.setState({
      product: {
        ...this.state.product,
        [name]: event.target.value,
      },
    });
  };

  checkBoxHandler = (name: any) => (event: any) => {
    this.setState({
      product: {
        ...this.state.product,
        [name]: event.target.checked,
      },
    });
  };

  createCommand = () => {
    this.setState({
      showModal: true,
      product: this.newProduct,
      command: Ecommand.create,
    });
    // this.setShowModal(true);
  };

  updateCommand = async (id: number) => {
    await ProductsService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            showModal: true,
            product: result.result,
            command: Ecommand.update,
          });
          this.loadProducts();
        } else {
          alert("Error result" + result.result);
        }
      })
      .catch((error) => {
        alert("Error result" + error);
      });
  };

  openGallery = async (productId: number) => {
    this.setState({
      showGallery: true,
      product: {
        ...this.state.product,
        id: productId,
      },
    });
  };

  selectGalery = async (galleryId: number) => {
    const { id } = this.state.product;
    ProductsService.changeGallery(id, galleryId)
      .then((result) => {
        if (result.success) {
          this.setState({
            showGallery: false,
          });
          this.loadProducts();
        } else {
          alert("Error result " + result.result);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  changeStatusCommand = async (id: number) => {
    await ProductsService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            showModal: true,
            product: result.result,
            command: Ecommand.ChangeStatus,
          });
          this.loadProducts();
        } else {
          alert("Error result" + result.result);
        }
      })
      .catch((error) => {
        alert("Error result" + error);
      });
  };

  changeRowPerPage = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
    new Promise(() => {
      setTimeout(() => {
        this.loadProducts();
      }, 500);
    });
  };

  changeSearch = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
  };

  changeOrder = (order: string) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        orderBy: order,
      },
    });
    new Promise(() => {
      setTimeout(() => {
        this.loadProducts();
      }, 500);
    });
  };

  submitHandler = async () => {
    const { command, product } = this.state;
    if (command === Ecommand.create) {
      await ProductsService.post(this.state.product)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              product: this.newProduct,
            });
            this.loadProducts();
          } else {
            alert("Error result " + result.result);
          }
        })
        .catch((error) => {
          alert("Error error" + error);
        });
    } else if (command === Ecommand.update) {
      await ProductsService.update(product.id, product)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              product: this.newProduct,
            });
            this.loadProducts();
          } else {
            alert("Error result " + result.result);
          }
        })
        .catch((error) => {
          alert("Error error" + error);
        });
    } else if (command === Ecommand.ChangeStatus) {
      await ProductsService.changeStatus(product.id, product.active)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              product: this.newProduct,
            });
            this.loadProducts();
          } else {
            alert("Error result " + result.result);
          }
        })
        .catch((error) => {
          alert("Error error" + error);
        });
    }
  };

  render() {
    const {
      variants,
      categories,
      galleries,
      products,
      product,
      showModal,
      command,
      pagination,
      imagePreview,
      showGallery,
    } = this.state;
    const loopPages = () => {
      let content: any = [];
      for (let page = 1; page <= pagination.pages; page++) {
        content.push(<option value={page}>{page}</option>);
      }
      return content;
    };
    return (
      <div>
        <div className="text-left text-3xl pt-5">Product</div>
        <span>{JSON.stringify(product)}</span>
        <div className="flex" aria-label="Button">
          <button
            className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded focus:shadow-outline hover:bg-green-800"
            onClick={() => this.createCommand()}
          >
            Create New
          </button>
        </div>
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th colSpan={1} scope="col" className="px-6 py-3 w-14 h-14">
                {/* grow flex-none  */}
                Search
              </th>
              <th colSpan={4} scope="col" className="px-6 py-3 w-14 h-14">
                <input
                  type="text"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  required
                  value={pagination.search}
                  onChange={this.changeSearch("search")}
                />
              </th>
              <th colSpan={1} scope="col" className="px-6 py-3 w-14 h-14">
                <button
                  className="h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg rounded-r-lg focus:shadow-outline hover:bg-green-800"
                  onClick={() => this.loadProducts()}
                >
                  Filter
                </button>
              </th>
              <th colSpan={3} scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowPerPage("sort")}
                >
                  <option value="0">Asc</option>
                  <option value="1">Desc</option>
                </select>
              </th>
            </tr>
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Category/Variants
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Gallery
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Initial/Name
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Description
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("order")}
              >
                Price
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("stock")}
              >
                Stock
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Active
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {products.map((prod: IProducts) => {
              return (
                <tr
                  key={prod.id}
                  className="border-b dark:bg-gray-800 dark:border-gray-700"
                >
                  <td className="px-6 py-4">
                    {prod.variants.category.initial}/{prod.variants.initial}
                  </td>
                  <td className="px-6 py-4">
                    <img
                      src={
                        prod.gallery.base64Big
                          ? prod.gallery.base64Big
                          : imagePreview
                      }
                      onClick={() => this.openGallery(prod.id)}
                    ></img>
                  </td>
                  <td className="px-6 py-4">
                    {prod.initial}/{prod.name}
                  </td>
                  <td className="px-6 py-4">{prod.description}</td>
                  <td className="px-6 py-4">{prod.price}</td>
                  <td className="px-6 py-4">{prod.stock}</td>
                  <td className="px-6 py-4">
                    <div className="flex items-center">
                      <input
                        checked={prod.active}
                        id="checked-checkbox"
                        type="checkbox"
                        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                      />
                    </div>
                  </td>
                  <td className="px-4 py-4">
                    <div
                      className="inline-flex"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        className="h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800"
                        onClick={() => this.updateCommand(prod.id)}
                      >
                        Edit
                      </button>
                      <button
                        className="h-8 px-4 text-blue-100 transition-colors duration-150 bg-blue-700 rounded-r-lg focus:shadow-outline hover:bg-blue-800"
                        onClick={() => this.changeStatusCommand(prod.id)}
                      >
                        Status
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
          <tfoot className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <label className="block text-sm font-medium text-gray-900 dark:text-white">
                  Rows per page
                </label>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowPerPage("rows")}
                >
                  {config.rowsPerPage.map((o: number) => {
                    return <option value={o}>{o}</option>;
                  })}
                </select>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14"></th>
              <th scope="col" className="px-6 py-3 w-14 h-14"></th>
              <th scope="col" className="px-6 py-3 w-14 h-14"></th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Page:
              </th>
              <th colSpan={3} scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowPerPage("pageNum")}
                >
                  {loopPages()}
                </select>
              </th>
            </tr>
          </tfoot>
        </table>
        {showModal ? (
          <div className="fixed inset-0 z-50 overflow-hidden">
            <div className="flex justify-center items-center min-h-screen px-4 pt-4 pb-20 text-center">
              <div className="w-full max-w-md bg-white dark:bg-gray-900 rounded-lg shadow-lg overflow-hidden overflow-y-auto">
                <div className="flex items-start justify-between p-5 border-b border-gray-300 dark:border-gray-700 rounded-t">
                  <h3 className="text-3xl text-gray-900 dark:text-white">
                    {command.valueOf()}
                  </h3>
                  <button
                    className="text-black float-right"
                    onClick={() => this.setShowModal(false)}
                  >
                    <span className="text-black opacity-70 h-6 w-6 text-xl block bg-gray-400 dark:bg-gray-700 py-0 rounded-full">
                      ×
                    </span>
                  </button>
                </div>
                <div className="p-6 overflow-y-auto max-h-96">
                  {/* Adjust max-h-96 to the maximum height you want before scrolling starts */}
                  <Form
                    categories={categories}
                    products={products}
                    variants={variants}
                    product={product}
                    command={command}
                    changeHandler={this.changeHandler}
                    checkBoxHandler={this.checkBoxHandler}
                  />
                </div>
                <div className="flex items-center justify-end p-6 border-t border-gray-300 dark:border-gray-700 rounded-b">
                  <button
                    className="mx-2 h-10 px-4 text-white transition-colors duration-300 bg-green-600 rounded-lg focus:outline-none hover:bg-green-700"
                    onClick={() => this.setShowModal(false)}
                  >
                    Close
                  </button>
                  <button
                    className="mx-2 h-10 px-4 text-white transition-colors duration-300 bg-blue-600 rounded-lg focus:outline-none hover:bg-blue-700"
                    onClick={() => this.submitHandler()}
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        {showGallery ? (
          <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl ">
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t ">
                  <h3 className="text-3xl text-gray-900 dark:text-white">
                    Gallery
                  </h3>
                  <button
                    className="bg-transparent border-0 text-black float-right"
                    onClick={() => this.setShowModal(false)}
                  >
                    <span className="text-black opacity-7 h-6 w-6 text-xl block bg-gray-400 py-0 rounded-full">
                      x
                    </span>
                  </button>
                </div>
                <div className="relative p-6 flex-auto">
                  <GalleryGrid selectGalery={this.selectGalery} />
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
