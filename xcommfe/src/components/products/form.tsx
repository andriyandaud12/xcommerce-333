import React from "react";
import { IProducts } from "../../interface/iProducts";
import { Ecommand } from "../../enums/eCommand";
import { IVariants } from "../../interface/iVariants";
import { ICategory } from "../../interface/iCategory";

interface IProps {
  variants: IVariants[];
  categories: ICategory[];
  products: IProducts[];
  product: IProducts;
  command: Ecommand;
  changeHandler: any;
  checkBoxHandler: any;
}

interface IState {}

export default class Products extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = this.props;
  }

  render() {
    const {
      categories,
      product,
      variants,
      command,
      changeHandler,
      checkBoxHandler,
    } = this.props;
    return (
      <>
        <form>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Variant
            </label>
            <select
              id="categoryId"
              disabled={command === Ecommand.ChangeStatus}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              value={product.categoryId}
              onChange={changeHandler("categoryId")}
            >
              <option selected value="0">
                Choose a Category
              </option>
              {categories.map((o: ICategory) => {
                return <option value={o.id}>{o.name}</option>;
              })}
            </select>
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Variant
            </label>
            <select
              id="variantId"
              disabled={command === Ecommand.ChangeStatus}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              value={product.variantId}
              onChange={changeHandler("variantId")}
            >
              <option selected value="0">
                Choose a Variants
              </option>
              {variants.map((o: IVariants) => {
                return <option value={o.id}>{o.name}</option>;
              })}
            </select>
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Initial
            </label>
            <input
              readOnly={command === Ecommand.ChangeStatus}
              type="text"
              id="initial"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              value={product.initial}
              onChange={changeHandler("initial")}
            />
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Name
            </label>
            <input
              readOnly={command === Ecommand.ChangeStatus}
              type="text"
              id="name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              value={product.name}
              onChange={changeHandler("name")}
            />
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Description
            </label>
            <input
              readOnly={command === Ecommand.ChangeStatus}
              type="text"
              id="name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              value={product.description}
              onChange={changeHandler("description")}
            />
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Price
            </label>
            <input
              readOnly={command === Ecommand.ChangeStatus}
              type="number"
              id="name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              value={product.price}
              onChange={changeHandler("price")}
            />
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Stock
            </label>
            <input
              readOnly={command === Ecommand.ChangeStatus}
              type="number"
              id="name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              value={product.stock}
              onChange={changeHandler("stock")}
            />
          </div>
          <div className="mb-6">
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Gallery
            </label>
            <input
              readOnly={command === Ecommand.ChangeStatus}
              type="number"
              id="galleryId"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
              value={product.galleryId}
              onChange={changeHandler("galleryId")}
            />
          </div>
          {command === Ecommand.ChangeStatus ? (
            <div className="flex items-start mb-6">
              <div className="flex items-center h-5">
                <input
                  type="checkbox"
                  value=""
                  className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800"
                  required
                  checked={product.active}
                  onChange={checkBoxHandler("active")}
                />
              </div>
              <label className="ms-2 text-sm font-medium text-gray-900 dark:text-gray-700">
                Is Active
              </label>
            </div>
          ) : null}
        </form>
      </>
    );
  }
}
