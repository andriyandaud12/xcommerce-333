import React from "react";
import { Link, Route, Routes, Router, RouteProps } from "react-router-dom";

import { Home } from "../home";
import { Category } from "../category";
import { Variants } from "../variants";
import { Products } from "../products";
import { Gallery } from "../gallery";
import { Authentication } from "../auth";
import { Order } from "../order";
import { ProtectedRoute } from "./protectedRoute";
import { Restiricted } from "./restricted";
import Header from "./header";
import { AccountModel } from "../models/accountModel";

interface IProps {
  logged: boolean;
  changeLoggedHandler: any;
  account: AccountModel;
}

interface IState {}

export default class SideBar extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
  }

  render() {
    const { logged, account, changeLoggedHandler } = this.props;
    return (
      <>
        <Header account={account} logged={logged} />
        <div className="flex h-screen bg-gray-200">
          {/* Sidebar */}
          <div className="flex-shrink-0 w-40 bg-gray-800 text-white">
            {/* Sidebar Content */}
            <div className="h-full flex flex-col">
              {/* Sidebar Header */}
              <div className="p-4 bg-gray-700">
                <h1 className="text-2xl font-semibold text-white">Sidebar</h1>
              </div>
              {/* Sidebar Links */}
              <nav className="flex-1 overflow-y-auto">
                <Link to="/" className="block p-4 hover:bg-gray-700">
                  Home
                </Link>
                {logged ? (
                  <>
                    {account.roles.map((o) => {
                      return (
                        <a
                          href="#"
                          className="flex items-center p-2 space-x-3 rounded-md block p-4 hover:bg-gray-700"
                        >
                          <span className="text-gray-100">
                            <Link to={`/${o}`}>{o}</Link>
                          </span>
                        </a>
                      );
                    })}
                  </>
                ) : null}

                <Link to="/auth" className="block p-4 hover:bg-gray-700">
                  {logged ? "Log Out" : "Log In"}
                </Link>
              </nav>
            </div>
          </div>
          {/* Main Content */}
          <div className="flex-1 p-8 overflow-y-auto">
            <Routes>
              <Route path="/" Component={Home} />
              <Route element={<ProtectedRoute />} />
              <Route
                element={
                  <ProtectedRoute
                    isAllowed={account.roles.indexOf("Categories") > -1}
                  />
                }
              >
                <Route path="/categories" element={<Category />} />
              </Route>
              <Route
                element={
                  <ProtectedRoute
                    isAllowed={account.roles.indexOf("Variants") > -1}
                  />
                }
              >
                <Route path="/variants" element={<Variants />} />
              </Route>
              <Route
                element={
                  <ProtectedRoute
                    isAllowed={account.roles.indexOf("Products") > -1}
                  />
                }
              >
                <Route path="/products" element={<Products />} />
              </Route>
              <Route
                element={
                  <ProtectedRoute
                    isAllowed={account.roles.indexOf("Galleries") > -1}
                  />
                }
              >
                <Route path="/galleries" element={<Gallery />} />
              </Route>
              <Route
                element={
                  <ProtectedRoute
                    isAllowed={account.roles.indexOf("Orders") > -1}
                  />
                }
              >
                <Route path="/orders" element={<Order />} />
              </Route>
              <Route
                path="/auth"
                element={
                  <Authentication
                    logged={logged}
                    changeLoggedHandler={changeLoggedHandler}
                  />
                }
              />
              <Route path="/restricted" Component={Restiricted} />
            </Routes>
          </div>
        </div>
      </>
    );
  }
}
