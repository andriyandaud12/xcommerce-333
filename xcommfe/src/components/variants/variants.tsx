import React from "react";
import Form from "../variants/form";
import { config } from "../../configurations/config";
import { IVariants } from "../../interface/iVariants";
import { VariantsService } from "../../services/variantsService";

import { Ecommand } from "../../enums/eCommand";
import { CategoryService } from "../../services/categoryService";
import { ICategory } from "../../interface/iCategory";
import { IPagination } from "../../interface/iPagination";

interface IProps {}

interface IState {
  categories: ICategory[];
  variants: IVariants[];
  variant: IVariants;
  showModal: boolean;
  command: Ecommand;
  pagination: IPagination;
}

export default class variant extends React.Component<IProps, IState> {
  newPagination: IPagination = {
    pageNum: 1,
    rows: config.rowsPerPage[0],
    search: "",
    orderBy: "id",
    sort: 1,
    pages: 0,
  };

  newCategory: ICategory = {
    id: 0,
    initial: "",
    name: "",
    active: false,
  };

  newVariant: IVariants = {
    id: 0,
    categoryId: 0,
    category: this.newCategory,
    initial: "",
    name: "",
    active: false,
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      categories: [],
      variants: [],
      variant: this.newVariant,
      showModal: false,
      command: Ecommand.create,
      pagination: this.newPagination,
    };
  }

  componentDidMount(): void {
    this.loadVariants();
  }

  loadVariants = async () => {
    const { pagination } = this.state;
    const result = await VariantsService.getAll(pagination);
    if (result.success) {
      CategoryService.getAllCat().then((catResult) => {
        console.log(catResult);
        if (catResult.success) {
          this.setState({
            categories: catResult.result.data,
          });
        }
      });
      this.setState({
        variants: result.result,
        pagination: {
          ...this.state.pagination,
          pages: result.pages,
        },
      });
    }
    console.log(result);
  };

  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
    });
    console.log(this.state.showModal);
  };

  changeHandler = (name: any) => (event: any) => {
    this.setState({
      variant: {
        ...this.state.variant,
        [name]: event.target.value,
      },
    });
  };

  checkBoxHandler = (name: any) => (event: any) => {
    this.setState({
      variant: {
        ...this.state.variant,
        [name]: event.target.checked,
      },
    });
  };

  createCommand = () => {
    this.setState({
      showModal: true,
      variant: this.newVariant,
      command: Ecommand.create,
    });
    // this.setShowModal(true);
  };

  updateCommand = async (id: number) => {
    await VariantsService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            showModal: true,
            variant: result.result,
            command: Ecommand.update,
          });
          this.loadVariants();
        } else {
          alert("Error result" + result.result);
        }
      })
      .catch((error) => {
        alert("Error result" + error);
      });
  };

  changeStatusCommand = async (id: number) => {
    await VariantsService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            showModal: true,
            variant: result.result,
            command: Ecommand.ChangeStatus,
          });
          this.loadVariants();
        } else {
          alert("Error result" + result.result);
        }
      })
      .catch((error) => {
        alert("Error result" + error);
      });
  };

  changeRowsPerPage = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
    new Promise(() => {
      setTimeout(() => {
        this.loadVariants();
      }, 500);
    });
  };

  changeSearch = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
  };

  changeOrder = (column: string) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        orderBy: column,
      },
    });
    new Promise(() => {
      setTimeout(() => {
        this.loadVariants();
      }, 500);
    });
  };

  submitHandler = async () => {
    const { command, variant } = this.state;
    if (command === Ecommand.create) {
      await VariantsService.post(this.state.variant)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              variant: this.newVariant,
            });
            this.loadVariants();
          } else {
            alert("Error result" + result.result);
          }
        })
        .catch((error) => {
          alert("Error result" + error);
        });
    } else if (command === Ecommand.update) {
      await VariantsService.update(variant.id, variant)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              variant: this.newVariant,
            });
            this.loadVariants();
          } else {
            alert("Error result" + result.result);
          }
        })
        .catch((error) => {
          alert("Error result" + error);
        });
    } else if (command === Ecommand.ChangeStatus) {
      await VariantsService.changeStatus(variant.id, variant.active)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              variant: this.newVariant,
            });
            this.loadVariants();
          } else {
            alert("Error result" + result.result);
          }
        })
        .catch((error) => {
          alert("Error result" + error);
        });
    }
  };

  render() {
    const { categories, variants, variant, showModal, command, pagination } =
      this.state;
    const loopPages = () => {
      let content: any = [];
      for (let page = 1; page <= pagination.pages; page++) {
        content.push(<option value={page}>{page}</option>);
      }
      return content;
    };
    return (
      <div>
        <div className="text-left text-4xl pt-5 py-5">Variants</div>
        <span>{JSON.stringify(variant)}</span>
        <div className="flex" aria-label="Button">
          <button
            className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 
          bg-green-700 rounded focus:shadow-outline hover:bg-green-800"
            onClick={() => this.createCommand()}
          >
            Create New
          </button>
        </div>
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Search:
              </th>
              <th colSpan={2} scope="col" className="px-6 py-3 w-14 h-14">
                <input
                  // readOnly={command === Ecommand.ChangeStatus}
                  type="text"
                  id="search"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  required
                  value={pagination.search}
                  onChange={this.changeSearch("search")}
                />
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <button
                  className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded focus:shadow-outline hover:bg-green-800"
                  onClick={() => this.loadVariants()}
                >
                  Filter
                </button>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowsPerPage("sort")}
                >
                  <option value="0">Asc</option>
                  <option value="1">Desc</option>
                </select>
              </th>
            </tr>
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th scope="col" className="px-6 py-3 w-14 h-14">
                CategoryId
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("initial")}
              >
                Initial
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("name")}
              >
                Name
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Active
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {variants?.map((cat: IVariants) => {
              return (
                <tr
                  key={cat.id}
                  className="border-b dark:bg-gray-800 dark:border-gray-700"
                >
                  <td className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                    {cat.category.initial}
                  </td>
                  <td className="px-6 py-4">{cat.initial}</td>
                  <td className="px-6 py-4">{cat.name}</td>
                  <td className="px-6 py-4">
                    <div className="flex items-center">
                      <input
                        checked={cat.active}
                        id="checked-checkbox"
                        type="checkbox"
                        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                      />
                    </div>
                  </td>
                  <td className="px-6 py-4">
                    <div
                      className="inline-flex"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        className="h-10 px-5 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800"
                        onClick={() => this.updateCommand(cat.id)}
                      >
                        Edit
                      </button>
                      <button
                        className="h-10 px-5 text-blue-100 transition-colors duration-150 bg-blue-700 rounded-r-lg focus:shadow-outline hover:bg-blue-800"
                        onClick={() => this.changeStatusCommand(cat.id)}
                      >
                        Status
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
          <tfoot className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                  Rows per page:
                </label>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onClick={this.changeRowsPerPage("rows")}
                >
                  {config.rowsPerPage.map((cat: number) => {
                    return <option value={cat}>{cat}</option>;
                  })}
                </select>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Page:
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowsPerPage("pageNum")}
                >
                  {loopPages()}
                </select>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14"></th>
            </tr>
          </tfoot>
        </table>
        {showModal ? (
          <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none ">
            <div className="relative w-auto my-6 mx-auto max-w-3xl ">
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t ">
                  <h3 className="text-3xl text-gray-900 dark:text-white">
                    {command.valueOf()}
                  </h3>
                  <button
                    className="bg-transparent border-0 text-black float-right"
                    onClick={() => this.setShowModal(false)}
                  >
                    <span className="text-black opacity-7 h-6 w-6 text-xl block bg-gray-400 py-0 rounded-full">
                      x
                    </span>
                  </button>
                </div>
                <div className="relative p-6 flex-auto">
                  <Form
                    categories={categories}
                    variant={variant}
                    command={command}
                    changeHandler={this.changeHandler}
                    checkBoxHandler={this.checkBoxHandler}
                  />
                </div>
                <div
                  className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b"
                  role="group"
                  aria-label="Button group"
                >
                  <button
                    className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800"
                    onClick={() => this.setShowModal(false)}
                  >
                    Close
                  </button>
                  <button
                    className="my-8 justify-start h-8 px-4 text-blue-100 transition-colors duration-150 bg-blue-700 rounded-r-lg focus:shadow-outline hover:bg-blue-800"
                    onClick={() => this.submitHandler()}
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
