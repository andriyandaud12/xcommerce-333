import { ICategory } from "./iCategory";
import { IGallery } from "./iGallery";
import { IVariants } from "./iVariants";

export interface IProducts extends ICategory {
  categoryId: number;
  variantId: number;
  galleryId: number;
  gallery: IGallery;
  variants: IVariants;
  description: string;
  price: number;
  stock: number;
}
