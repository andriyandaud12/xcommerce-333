import { ICategory } from "./iCategory";

export interface IVariants extends ICategory {
  categoryId: number;
  category: ICategory;
}
