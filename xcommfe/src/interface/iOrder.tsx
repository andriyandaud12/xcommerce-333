export interface IOrder {
  id: number;
  reference: string;
  amount: number;
  quantity: number;
  price: number;
  active: boolean;
}
